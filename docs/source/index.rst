.. serialtools documentation master file, created by
   sphinx-quickstart on Wed Jun 21 11:17:06 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. py:currentmodule:: serialtools


======================================
Welcome to serialtools' documentation!
======================================


Reference
=========

.. toctree::
   :maxdepth: 2

   serialtools
   serialtools.apps.decode
   serialtools.apps.dump
   serialtools.apps.__init__
   serialtools.apps.rawsplit
   serialtools.apps.send
   serialtools.bus
   serialtools.database
   serialtools.database_config
   serialtools.__main__


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Links
=====

* `Source code <https://gitlab.com/erzo/serialtools>`_
* `Bug tracker <https://gitlab.com/erzo/serialtools/-/issues>`_
* `Change log <https://gitlab.com/erzo/serialtools/-/tags>`_
* `PyPI <https://pypi.org/project/serialtools/>`_
